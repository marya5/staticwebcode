package com.amdocs;

import static org.junit.Assert.*;
import org.junit.Test;

public class CalculatorTest {

  @Test
  public void testAdd() throws Exception
  {
	final int result = new Calculator().add();
	assertEquals("Add", result, 9 );
  }


  @Test
  public void testSub() throws Exception
  {
       final  int result = new Calculator().sub();
        assertEquals("Sub", result, 3 );
  }

}

