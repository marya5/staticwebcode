package com.amdocs;

import static org.junit.Assert.*;
import org.junit.Test;

public class IncrementTest {
  
@Test
public void getCounterTest(){
 final int result= new Increment().getCounter();
 assertEquals("getCounter",1,result);
}

@Test
public void decreasecounterTest(){
final int result= new Increment().decreasecounter(2);
assertEquals("decreaseCounter", 2, result);
}

}
